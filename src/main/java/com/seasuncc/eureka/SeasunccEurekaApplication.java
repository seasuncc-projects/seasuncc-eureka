package com.seasuncc.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 服务的注册中心
 *
 * @author shidoudou
 * @date 2020/9/14
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableEurekaServer
public class SeasunccEurekaApplication {
    public static void main(String[] args) {
        SpringApplication.run(SeasunccEurekaApplication.class, args);
    }

}
